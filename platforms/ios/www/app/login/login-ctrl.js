/**
 * Created by mhill168 on 29/06/2015.
 */


(function () {

    'use strict';

    angular.module('sentinelApp').controller('LoginCtrl', ['$scope', '$state', '$ionicUser', '$ionicModal', '$timeout', '$localStorage', LoginCtrl]);

    function LoginCtrl($scope, $state, $ionicUser, $ionicModal, $timeout, $localStorage) {

        // Form data for the login modal
        $scope.authorization = {
            username: '',
            password: ''
        };

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('app/login/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function (form) {
            if (form.$valid) {

                $ionicUser.identify({
                    user_id: '0',
                    name: $scope.authorization.username,
                    password: $scope.authorization.password,
                    message: 'I come from planet Ion'
                });

                $localStorage.setObject('user', {
                    name: $scope.authorization.username,
                    password: $scope.authorization.password
                });

                var getObj = $localStorage.getObject('user');
                console.log(getObj);

                $state.go('app.profile');
                $scope.closeLogin();

            }

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function () {
                // reset the users input fields
                $scope.authorization = {};
            }, 1000);
        };
    }

})();

