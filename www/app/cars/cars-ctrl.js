/**
 * Created by mhill168 on 29/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('CarsCtrl',  ['$scope', 'theftData', CarsCtrl]);

    function CarsCtrl($scope, theftData) {

        var vm = this;

        theftData.getFeedData().then(function (data) {
            vm.cars = data.cars;
            console.log(vm.cars);
        });

        // Refreshing the data
        $scope.doRefresh = function() {
            console.log("Refreshing the data....");
            // update the data
            theftData.getFeedData().then(function (newData) {
                vm.cars = newData.cars;
            });
            //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.addCar = function () {
            vm.cars.push({title: 'Added Car', image: "LastOfTheGreats.jpg", id: 7});
        }
    }

})();
