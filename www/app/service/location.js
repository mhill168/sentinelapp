/**
 * Created by mhill168 on 26/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp.location', [])

        .factory('geoLocation', function ($localStorage) {
            return {
                setGeolocation: function (latitude, longitude) {
                    var _position = {
                        latitude: latitude,
                        longitude: longitude
                    };
                    $localStorage.setObject('geoLocation', _position)
                },
                getGeolocation: function () {
                    return glocation = {
                        lat: $localStorage.getObject('geoLocation').latitude,
                        lng: $localStorage.getObject('geoLocation').longitude
                    }
                }
            }
        });

})();

