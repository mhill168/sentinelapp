// Ionic Sentinel App

angular.module('sentinelApp', [
    'ionic',
    'ionic.service.core',
    'ionic.service.push',
    'ionic.service.analytics',
    'sentinelApp.location',
    'sentinelApp.storage',
    'ngCordova',
    'ngMessages'
])

    .config(function ($compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    })

    .run(function ($ionicPlatform, $ionicAnalytics) {
        $ionicPlatform.ready(function () {

            // Run the Analytics
            $ionicAnalytics.register();

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicAppProvider) {

        // Identify app
        $ionicAppProvider.identify({
            app_id: '94a0ed3c',
            api_key: '1f83ab3787aeb7840df727103825926c57ef9b61b5402d1e',
            dev_push: true // set to false when in prod mode
        });

        $stateProvider

            .state('intro', {
                url: '/intro',
                templateUrl: 'app/intro/intro.html'
            })

            .state('login', {
                url: '/login',
                templateUrl: "app/login/login.html",
                controller: 'LoginCtrl'
            })

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "app/menu/menu.html",
                controller: 'LoginCtrl'
            })

            .state('app.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: "app/profile/profile.html"
                    }
                }
            })

            .state('app.my-cars', {
                url: "/my-cars",
                views: {
                    'menuContent': {
                        templateUrl: "app/cars/my-cars.html"
                    }
                }
            })

            .state('app.my-car', {
                url: "/my-cars/:carId",
                views: {
                    'menuContent': {
                        templateUrl: "app/cars/my-car.html"
                    }
                }
            })

            .state('app.my-places', {
                url: "/my-places",
                views: {
                    'menuContent': {
                        templateUrl: "app/places/my-places.html"
                    }
                }
            })

            .state('app.my-place', {
                url: "/my-places/:city",
                views: {
                    'menuContent': {
                        templateUrl: "app/places/my-place.html"
                    }
                }
            })

            .state('app.notifications', {
                url: "/notifications",
                views: {
                    'menuContent': {
                        templateUrl: "app/notifications/notifications.html"
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/profile');

    });
