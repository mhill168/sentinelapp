3.2.1 (Media Mark)
e5bb7365bf7ba866eab76c118627aeed3c6f72fe
o:Sass::Tree::RootNode
:
@linei:@options{ :@template"�/**
 * Forms
 * --------------------------------------------------
 */

// Make all forms have space below them
form {
  margin: 0 0 $line-height-base;
}

// Groups of fields with labels on top (legends)
legend {
  display: block;
  margin-bottom: $line-height-base;
  padding: 0;
  width: 100%;
  border: $input-border-width solid $input-border;
  color: $dark;
  font-size: $font-size-base * 1.5;
  line-height: $line-height-base * 2;

  small {
    color: $stable;
    font-size: $line-height-base * .75;
  }
}

// Set font for forms
label,
input,
button,
select,
textarea {
  @include font-shorthand($font-size-base, normal, $line-height-base); // Set size, weight, line-height here
}
input,
button,
select,
textarea {
  font-family: $font-family-base; // And only set font-family here for those that need it (note the missing label element)
}


// Input List
// -------------------------------

.item-input {
  @include display-flex();
  @include align-items(center);
  position: relative;
  overflow: hidden;
  padding: 6px 0 5px 16px;

  input {
    @include border-radius(0);
    @include flex(1, 220px);
    @include appearance(none);
    margin: 0;
    padding-right: 24px;
    background-color: transparent;
  }

  .button .icon {
    @include flex(0, 0, 24px);
    position: static;
    display: inline-block;
    height: auto;
    text-align: center;
    font-size: 16px;
  }

  .button-bar {
    @include border-radius(0);
    @include flex(1, 0, 220px);
    @include appearance(none);
  }

  .icon {
    min-width: 14px;
  }
}
// prevent flex-shrink on WP
.platform-windowsphone .item-input input{
  flex-shrink: 1;
}

.item-input-inset {
  @include display-flex();
  @include align-items(center);
  position: relative;
  overflow: hidden;
  padding: ($item-padding / 3) * 2;
}

.item-input-wrapper {
  @include display-flex();
  @include flex(1, 0);
  @include align-items(center);
  @include border-radius(4px);
  padding-right: 8px;
  padding-left: 8px;
  background: #eee;
}

.item-input-inset .item-input-wrapper input {
  padding-left: 4px;
  height: 29px;
  background: transparent;
  line-height: 18px;
}

.item-input-wrapper ~ .button {
  margin-left: ($item-padding / 3) * 2;
}

.input-label {
  display: table;
  padding: 7px 10px 7px 0px;
  max-width: 200px;
  width: 35%;
  color: $input-label-color;
  font-size: 16px;
}

.placeholder-icon {
  color: #aaa;
  &:first-child {
    padding-right: 6px;
  }
  &:last-child {
    padding-left: 6px;
  }
}

.item-stacked-label {
  display: block;
  background-color: transparent;
  box-shadow: none;

  .input-label, .icon {
    display: inline-block;
    padding: 4px 0 0 0px;
    vertical-align: middle;
  }
}

.item-stacked-label input,
.item-stacked-label textarea {
  @include border-radius(2px);
  padding: 4px 8px 3px 0;
  border: none;
  background-color: $input-bg;
}
.item-stacked-label input {
  overflow: hidden;
  height: $line-height-computed + $font-size-base + 12px;
}

.item-floating-label {
  display: block;
  background-color: transparent;
  box-shadow: none;

  .input-label {
    position: relative;
    padding: 5px 0 0 0;
    opacity: 0;
    top: 10px;
    @include transition(opacity .15s ease-in, top .2s linear);

    &.has-input {
      opacity: 1;
      top: 0;
      @include transition(opacity .15s ease-in, top .2s linear);
    }
  }
}


// Form Controls
// -------------------------------

// Shared size and type resets
textarea,
input[type="text"],
input[type="password"],
input[type="datetime"],
input[type="datetime-local"],
input[type="date"],
input[type="month"],
input[type="time"],
input[type="week"],
input[type="number"],
input[type="email"],
input[type="url"],
input[type="search"],
input[type="tel"],
input[type="color"] {
  display: block;
  padding-top: 2px;
  padding-left: 0;
  height: $line-height-computed + $font-size-base;
  color: $input-color;
  vertical-align: middle;
  font-size: $font-size-base;
  line-height: $font-size-base + 2;
}

.platform-ios,
.platform-android {
  input[type="datetime-local"],
  input[type="date"],
  input[type="month"],
  input[type="time"],
  input[type="week"] {
    padding-top: 8px;
  }
}

.item-input {
  input,
  textarea {
    width: 100%;
  }
}

textarea {
  padding-left: 0;
  @include placeholder($input-color-placeholder, -3px);
}

// Reset height since textareas have rows
textarea {
  height: auto;
}

// Everything else
textarea,
input[type="text"],
input[type="password"],
input[type="datetime"],
input[type="datetime-local"],
input[type="date"],
input[type="month"],
input[type="time"],
input[type="week"],
input[type="number"],
input[type="email"],
input[type="url"],
input[type="search"],
input[type="tel"],
input[type="color"] {
  border: 0;
}

// Position radios and checkboxes better
input[type="radio"],
input[type="checkbox"] {
  margin: 0;
  line-height: normal;
}

// Reset width of input images, buttons, radios, checkboxes
.item-input {
  input[type="file"],
  input[type="image"],
  input[type="submit"],
  input[type="reset"],
  input[type="button"],
  input[type="radio"],
  input[type="checkbox"] {
    width: auto; // Override of generic input selector
  }
}

// Set the height of file to match text inputs
input[type="file"] {
  line-height: $input-height-base;
}

// Text input classes to hide text caret during scroll
.previous-input-focus,
.cloned-text-input + input,
.cloned-text-input + textarea {
  position: absolute !important;
  left: -9999px;
  width: 200px;
}


// Placeholder
// -------------------------------
input,
textarea {
  @include placeholder();
}


// DISABLED STATE
// -------------------------------

// Disabled and read-only inputs
input[disabled],
select[disabled],
textarea[disabled],
input[readonly]:not(.cloned-text-input),
textarea[readonly]:not(.cloned-text-input),
select[readonly] {
  background-color: $input-bg-disabled;
  cursor: not-allowed;
}
// Explicitly reset the colors here
input[type="radio"][disabled],
input[type="checkbox"][disabled],
input[type="radio"][readonly],
input[type="checkbox"][readonly] {
  background-color: transparent;
}
:@has_childrenT:@children[4o:Sass::Tree::CommentNode
;i;@;
[ :
@type:normal:@value["K/**
 * Forms
 * --------------------------------------------------
 */o;
;i;@;
[ ;:silent;["//* Make all forms have space below them */o:Sass::Tree::RuleNode:
@tabsi ;@:
@rule["	form:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
;@;i:@subject0;[o:Sass::Selector::Element	;@:@namespace0:
@name["	form;i:@sourceso:Set:
@hash{ ;	T;i;
[o:Sass::Tree::PropNode;i ;["margin;@:@prop_syntax:new;o:Sass::Script::List	;i;@:@separator:
space;[o:Sass::Script::Number:@numerator_units[ ;i;@:@original"0;i :@denominator_units[ o;&;'[ ;i;@;("0;i ;)@)o:Sass::Script::Variable	;i;"line-height-base;@:@underscored_name"line_height_base;i;
[ o;
;i;@;
[ ;;;["8/* Groups of fields with labels on top (legends) */o;;i ;@;["legend;o;;" ;i;[o;;[o;
;@9;i;0;[o;	;@9;0;["legend;i;o;;{ ;	T;i;
[o; ;i ;["display;@;!;";o:Sass::Script::String;@;"
block;:identifier;i;
[ o; ;i ;["margin-bottom;@;!;";o;*	;i;"line-height-base;@;+"line_height_base;i;
[ o; ;i ;["padding;@;!;";o;,;@;"0;;-;i;
[ o; ;i ;["
width;@;!;";o;,;@;"	100%;;-;i;
[ o; ;i ;["border;@;!;";o;#	;i;@;$;%;[o;*	;i;"input-border-width;@;+"input_border_widtho;,	;i;@;"
solid;;-o;*	;i;"input-border;@;+"input_border;i;
[ o; ;i ;["
color;@;!;";o;*	;i;"	dark;@;+"	dark;i;
[ o; ;i ;["font-size;@;!;";o:Sass::Script::Operation
:@operator:
times;i;@:@operand1o;*	;i;"font-size-base;@;+"font_size_base:@operand2o;&;'[ ;i;@;("1.5;f1.5;)@);i;
[ o; ;i ;["line-height;@;!;";o;.
;/;0;i;@;1o;*	;i;"line-height-base;@;+"line_height_base;2o;&;'[ ;i;@;("2;i;)@);i;
[ o;;i ;@;["
small;o;;" ;i;[o;;[o;
;@�;i;0;[o;	;@�;0;["
small;i;o;;{ ;	T;i;
[o; ;i ;["
color;@;!;";o;*	;i;"stable;@;+"stable;i;
[ o; ;i ;["font-size;@;!;";o;.
;/;0;i;@;1o;*	;i;"line-height-base;@;+"line_height_base;2o;&;'[ ;i;@;("	0.75;f	0.75;)@);i;
[ o;
;i!;@;
[ ;;;["/* Set font for forms */o;;i ;@;["+label,
input,
button,
select,
textarea;o;;" ;i&;[
o;;[o;
;@�;i&;0;[o;	;@�;0;["
label;i&;o;;{ o;;["
o;
;@�;i&;0;[o;	;@�;0;["
input;i&;o;;{ o;;["
o;
;@�;i&;0;[o;	;@�;0;["button;i&;o;;{ o;;["
o;
;@�;i&;0;[o;	;@�;0;["select;i&;o;;{ o;;["
o;
;@�;i&;0;[o;	;@�;0;["textarea;i&;o;;{ ;	T;i&;
[o:Sass::Tree::MixinNode:
@args[o;*	;i';"font-size-base;@;+"font_size_baseo;,	;i';@;"normal;;-o;*	;i';"line-height-base;@;+"line_height_base;"font-shorthand;i';@;
[ :@splat0:@keywords{ o;
;i';@;
[ ;;;["-/* Set size, weight, line-height here */o;;i ;@;["$input,
button,
select,
textarea;o;;" ;i,;[	o;;[o;
;@�;i,;0;[o;	;@�;0;["
input;i,;o;;{ o;;["
o;
;@�;i,;0;[o;	;@�;0;["button;i,;o;;{ o;;["
o;
;@�;i,;0;[o;	;@�;0;["select;i,;o;;{ o;;["
o;
;@�;i,;0;[o;	;@�;0;["textarea;i,;o;;{ ;	T;i,;
[o; ;i ;["font-family;@;!;";o;*	;i-;"font-family-base;@;+"font_family_base;i-;
[ o;
;i-;@;
[ ;;;["`/* And only set font-family here for those that need it (note the missing label element) */o;
;i1;@;
[ ;;;["8/* Input List
 * ------------------------------- */o;;i ;@;[".item-input;o;;" ;i4;[o;;[o;
;@6;i4;0;[o:Sass::Selector::Class;@6;["item-input;i4;o;;{ ;	T;i4;
[o;3;4[ ;"display-flex;i5;@;
[ ;50;6{ o;3;4[o;,	;i6;@;"center;;-;"align-items;i6;@;
[ ;50;6{ o; ;i ;["position;@;!;";o;,;@;"relative;;-;i7;
[ o; ;i ;["overflow;@;!;";o;,;@;"hidden;;-;i8;
[ o; ;i ;["padding;@;!;";o;,;@;"6px 0 5px 16px;;-;i9;
[ o;;i ;@;["
input;o;;" ;i;;[o;;[o;
;@d;i;;0;[o;	;@d;0;["
input;i;;o;;{ ;	T;i;;
[o;3;4[o;&;'[ ;i<;@;("0;i ;)@);"border-radius;i<;@;
[ ;50;6{ o;3;4[o;&;'[ ;i=;@;("1;i;)@)o;&;'["px;i=;@;("
220px;i�;)[ ;"	flex;i=;@;
[ ;50;6{ o;3;4[o;,	;i>;@;"	none;;-;"appearance;i>;@;
[ ;50;6{ o; ;i ;["margin;@;!;";o;,;@;"0;;-;i?;
[ o; ;i ;["padding-right;@;!;";o;,;@;"	24px;;-;i@;
[ o; ;i ;["background-color;@;!;";o;,;@;"transparent;;-;iA;
[ o;;i ;@;[".button .icon;o;;" ;iD;[o;;[o;
;@�;iD;0;[o;7;@�;["button;iD;o;;{ o;
;@�;iD;0;[o;7;@�;["	icon;iD;o;;{ ;	T;iD;
[o;3;4[o;&;'[ ;iE;@;("0;i ;)@)o;&;'[ ;iE;@;("0;i ;)@)o;&;'["px;iE;@;("	24px;i;)[ ;"	flex;iE;@;
[ ;50;6{ o; ;i ;["position;@;!;";o;,;@;"static;;-;iF;
[ o; ;i ;["display;@;!;";o;,;@;"inline-block;;-;iG;
[ o; ;i ;["height;@;!;";o;,;@;"	auto;;-;iH;
[ o; ;i ;["text-align;@;!;";o;,;@;"center;;-;iI;
[ o; ;i ;["font-size;@;!;";o;,;@;"	16px;;-;iJ;
[ o;;i ;@;[".button-bar;o;;" ;iM;[o;;[o;
;@�;iM;0;[o;7;@�;["button-bar;iM;o;;{ ;	T;iM;
[o;3;4[o;&;'[ ;iN;@;("0;i ;)@);"border-radius;iN;@;
[ ;50;6{ o;3;4[o;&;'[ ;iO;@;("1;i;)@)o;&;'[ ;iO;@;("0;i ;)@)o;&;'["px;iO;@;("
220px;i�;)[ ;"	flex;iO;@;
[ ;50;6{ o;3;4[o;,	;iP;@;"	none;;-;"appearance;iP;@;
[ ;50;6{ o;;i ;@;["
.icon;o;;" ;iS;[o;;[o;
;@;iS;0;[o;7;@;["	icon;iS;o;;{ ;	T;iS;
[o; ;i ;["min-width;@;!;";o;,;@;"	14px;;-;iT;
[ o;
;iW;@;
[ ;;;["$/* prevent flex-shrink on WP */o;;i ;@;["-.platform-windowsphone .item-input input;o;;" ;iX;[o;;[o;
;@0;iX;0;[o;7;@0;["platform-windowsphone;iX;o;;{ o;
;@0;iX;0;[o;7;@0;["item-input;iX;o;;{ o;
;@0;iX;0;[o;	;@0;0;["
input;iX;o;;{ ;	T;iX;
[o; ;i ;["flex-shrink;@;!;";o;,;@;"1;;-;iY;
[ o;;i ;@;[".item-input-inset;o;;" ;i\;[o;;[o;
;@T;i\;0;[o;7;@T;["item-input-inset;i\;o;;{ ;	T;i\;
[
o;3;4[ ;"display-flex;i];@;
[ ;50;6{ o;3;4[o;,	;i^;@;"center;;-;"align-items;i^;@;
[ ;50;6{ o; ;i ;["position;@;!;";o;,;@;"relative;;-;i_;
[ o; ;i ;["overflow;@;!;";o;,;@;"hidden;;-;i`;
[ o; ;i ;["padding;@;!;";o;.
;/;0;ia;@;1o;.
;/:div;ia;@;1o;*	;ia;"item-padding;@;+"item_padding;2o;&;'[ ;ia;@;("3;i;)@);2o;&;'[ ;ia;@;("2;i;)@);ia;
[ o;;i ;@;[".item-input-wrapper;o;;" ;id;[o;;[o;
;@�;id;0;[o;7;@�;["item-input-wrapper;id;o;;{ ;	T;id;
[o;3;4[ ;"display-flex;ie;@;
[ ;50;6{ o;3;4[o;&;'[ ;if;@;("1;i;)@)o;&;'[ ;if;@;("0;i ;)@);"	flex;if;@;
[ ;50;6{ o;3;4[o;,	;ig;@;"center;;-;"align-items;ig;@;
[ ;50;6{ o;3;4[o;&;'["px;ih;@;("4px;i	;)[ ;"border-radius;ih;@;
[ ;50;6{ o; ;i ;["padding-right;@;!;";o;,;@;"8px;;-;ii;
[ o; ;i ;["padding-left;@;!;";o;,;@;"8px;;-;ij;
[ o; ;i ;["background;@;!;";o;,;@;"	#eee;;-;ik;
[ o;;i ;@;["0.item-input-inset .item-input-wrapper input;o;;" ;in;[o;;[o;
;@�;in;0;[o;7;@�;["item-input-inset;in;o;;{ o;
;@�;in;0;[o;7;@�;["item-input-wrapper;in;o;;{ o;
;@�;in;0;[o;	;@�;0;["
input;in;o;;{ ;	T;in;
[	o; ;i ;["padding-left;@;!;";o;,;@;"4px;;-;io;
[ o; ;i ;["height;@;!;";o;,;@;"	29px;;-;ip;
[ o; ;i ;["background;@;!;";o;,;@;"transparent;;-;iq;
[ o; ;i ;["line-height;@;!;";o;,;@;"	18px;;-;ir;
[ o;;i ;@;["".item-input-wrapper ~ .button;o;;" ;iu;[o;;[o;
;@;iu;0;[o;7;@;["item-input-wrapper;iu;o;;{ "~o;
;@;iu;0;[o;7;@;["button;iu;o;;{ ;	T;iu;
[o; ;i ;["margin-left;@;!;";o;.
;/;0;iv;@;1o;.
;/;8;iv;@;1o;*	;iv;"item-padding;@;+"item_padding;2o;&;'[ ;iv;@;("3;i;)@);2o;&;'[ ;iv;@;("2;i;)@);iv;
[ o;;i ;@;[".input-label;o;;" ;iy;[o;;[o;
;@+;iy;0;[o;7;@+;["input-label;iy;o;;{ ;	T;iy;
[o; ;i ;["display;@;!;";o;,;@;"
table;;-;iz;
[ o; ;i ;["padding;@;!;";o;,;@;"7px 10px 7px 0px;;-;i{;
[ o; ;i ;["max-width;@;!;";o;,;@;"
200px;;-;i|;
[ o; ;i ;["
width;@;!;";o;,;@;"35%;;-;i};
[ o; ;i ;["
color;@;!;";o;*	;i~;"input-label-color;@;+"input_label_color;i~;
[ o; ;i ;["font-size;@;!;";o;,;@;"	16px;;-;i;
[ o;;i ;@;[".placeholder-icon;o;;" ;i};[o;;[o;
;@`;i};0;[o;7;@`;["placeholder-icon;i};o;;{ ;	T;i};
[o; ;i ;["
color;@;!;";o;,;@;"	#aaa;;-;i~;
[ o;;i ;@;["&:first-child;o;;" ;i;[o;;[o;
;@v;i;0;[o:Sass::Selector::Parent;@v;io:Sass::Selector::Pseudo
;@v;["first-child;i;:
class:	@arg0;o;;{ ;	T;i;
[o; ;i ;["padding-right;@;!;";o;,;@;"6px;;-;i�;
[ o;;i ;@;["&:last-child;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;9;@�;i�o;:
;@�;["last-child;i�;;;;<0;o;;{ ;	T;i�;
[o; ;i ;["padding-left;@;!;";o;,;@;"6px;;-;i�;
[ o;;i ;@;[".item-stacked-label;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;7;@�;["item-stacked-label;i�;o;;{ ;	T;i�;
[	o; ;i ;["display;@;!;";o;,;@;"
block;;-;i�;
[ o; ;i ;["background-color;@;!;";o;,;@;"transparent;;-;i�;
[ o; ;i ;["box-shadow;@;!;";o;,;@;"	none;;-;i�;
[ o;;i ;@;[".input-label, .icon;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;7;@�;["input-label;i�;o;;{ o;;[o;
;@�;i�;0;[o;7;@�;["	icon;i�;o;;{ ;	T;i�;
[o; ;i ;["display;@;!;";o;,;@;"inline-block;;-;i�;
[ o; ;i ;["padding;@;!;";o;,;@;"4px 0 0 0px;;-;i�;
[ o; ;i ;["vertical-align;@;!;";o;,;@;"middle;;-;i�;
[ o;;i ;@;["<.item-stacked-label input,
.item-stacked-label textarea;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;7;@�;["item-stacked-label;i�;o;;{ o;
;@�;i�;0;[o;	;@�;0;["
input;i�;o;;{ o;;["
o;
;@�;i�;0;[o;7;@�;["item-stacked-label;i�;o;;{ o;
;@�;i�;0;[o;	;@�;0;["textarea;i�;o;;{ ;	T;i�;
[	o;3;4[o;&;'["px;i�;@;("2px;i;)[ ;"border-radius;i�;@;
[ ;50;6{ o; ;i ;["padding;@;!;";o;,;@;"4px 8px 3px 0;;-;i�;
[ o; ;i ;["border;@;!;";o;,;@;"	none;;-;i�;
[ o; ;i ;["background-color;@;!;";o;*	;i�;"input-bg;@;+"input_bg;i�;
[ o;;i ;@;[".item-stacked-label input;o;;" ;i�;[o;;[o;
;@6;i�;0;[o;7;@6;["item-stacked-label;i�;o;;{ o;
;@6;i�;0;[o;	;@6;0;["
input;i�;o;;{ ;	T;i�;
[o; ;i ;["overflow;@;!;";o;,;@;"hidden;;-;i�;
[ o; ;i ;["height;@;!;";o;.
;/:	plus;i�;@;1o;.
;/;=;i�;@;1o;*	;i�;"line-height-computed;@;+"line_height_computed;2o;*	;i�;"font-size-base;@;+"font_size_base;2o;&;'["px;i�;@;("	12px;i;)[ ;i�;
[ o;;i ;@;[".item-floating-label;o;;" ;i�;[o;;[o;
;@d;i�;0;[o;7;@d;["item-floating-label;i�;o;;{ ;	T;i�;
[	o; ;i ;["display;@;!;";o;,;@;"
block;;-;i�;
[ o; ;i ;["background-color;@;!;";o;,;@;"transparent;;-;i�;
[ o; ;i ;["box-shadow;@;!;";o;,;@;"	none;;-;i�;
[ o;;i ;@;[".input-label;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;7;@�;["input-label;i�;o;;{ ;	T;i�;
[o; ;i ;["position;@;!;";o;,;@;"relative;;-;i�;
[ o; ;i ;["padding;@;!;";o;,;@;"5px 0 0 0;;-;i�;
[ o; ;i ;["opacity;@;!;";o;,;@;"0;;-;i�;
[ o; ;i ;["top;@;!;";o;,;@;"	10px;;-;i�;
[ o;3;4[o;#	;i�;@;$;%;[o;,	;i�;@;"opacity;;-o;&;'["s;i�;@;("
0.15s;f	0.15;)[ o;,	;i�;@;"ease-in;;-o;#	;i�;@;$;%;[o;,	;i�;@;"top;;-o;&;'["s;i�;@;("	0.2s;f0.2;)[ o;,	;i�;@;"linear;;-;"transition;i�;@;
[ ;50;6{ o;;i ;@;["&.has-input;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;9;@�;i�o;7;@�;["has-input;i�;o;;{ ;	T;i�;
[o; ;i ;["opacity;@;!;";o;,;@;"1;;-;i�;
[ o; ;i ;["top;@;!;";o;,;@;"0;;-;i�;
[ o;3;4[o;#	;i�;@;$;%;[o;,	;i�;@;"opacity;;-o;&;'["s;i�;@;("
0.15s;f	0.15;)[ o;,	;i�;@;"ease-in;;-o;#	;i�;@;$;%;[o;,	;i�;@;"top;;-o;&;'["s;i�;@;("	0.2s;f0.2;)[ o;,	;i�;@;"linear;;-;"transition;i�;@;
[ ;50;6{ o;
;i�;@;
[ ;;;[";/* Form Controls
 * ------------------------------- */o;
;i�;@;
[ ;;;["&/* Shared size and type resets */o;;i ;@;["7textarea,
input[type="text"],
input[type="password"],
input[type="datetime"],
input[type="datetime-local"],
input[type="date"],
input[type="month"],
input[type="time"],
input[type="week"],
input[type="number"],
input[type="email"],
input[type="url"],
input[type="search"],
input[type="tel"],
input[type="color"];o;;" ;i�;[o;;[o;
;@;i�;0;[o;	;@;0;["textarea;i�;o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o:Sass::Selector::Attribute;@;/"=;["	type;0;i�:@flags0;[""text";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""password";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""datetime";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""datetime-local";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""date";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""month";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""time";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""week";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""number";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""email";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;["
"url";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""search";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;["
"tel";o;;{ o;;["
o;
;@;i�;0;[o;	;@;0;["
input;i�o;>;@;/"=;["	type;0;i�;?0;[""color";o;;{ ;	T;i�;
[o; ;i ;["display;@;!;";o;,;@;"
block;;-;i�;
[ o; ;i ;["padding-top;@;!;";o;,;@;"2px;;-;i�;
[ o; ;i ;["padding-left;@;!;";o;,;@;"0;;-;i�;
[ o; ;i ;["height;@;!;";o;.
;/;=;i�;@;1o;*	;i�;"line-height-computed;@;+"line_height_computed;2o;*	;i�;"font-size-base;@;+"font_size_base;i�;
[ o; ;i ;["
color;@;!;";o;*	;i�;"input-color;@;+"input_color;i�;
[ o; ;i ;["vertical-align;@;!;";o;,;@;"middle;;-;i�;
[ o; ;i ;["font-size;@;!;";o;*	;i�;"font-size-base;@;+"font_size_base;i�;
[ o; ;i ;["line-height;@;!;";o;.
;/;=;i�;@;1o;*	;i�;"font-size-base;@;+"font_size_base;2o;&;'[ ;i�;@;("2;i;)@);i�;
[ o;;i ;@;["%.platform-ios,
.platform-android;o;;" ;i�;[o;;[o;
;@9;i�;0;[o;7;@9;["platform-ios;i�;o;;{ o;;["
o;
;@9;i�;0;[o;7;@9;["platform-android;i�;o;;{ ;	T;i�;
[o;;i ;@;["zinput[type="datetime-local"],
  input[type="date"],
  input[type="month"],
  input[type="time"],
  input[type="week"];o;;" ;i�;[
o;;[o;
;@S;i�;0;[o;	;@S;0;["
input;i�o;>;@S;/"=;["	type;0;i�;?0;[""datetime-local";o;;{ o;;["
o;
;@S;i�;0;[o;	;@S;0;["
input;i�o;>;@S;/"=;["	type;0;i�;?0;[""date";o;;{ o;;["
o;
;@S;i�;0;[o;	;@S;0;["
input;i�o;>;@S;/"=;["	type;0;i�;?0;[""month";o;;{ o;;["
o;
;@S;i�;0;[o;	;@S;0;["
input;i�o;>;@S;/"=;["	type;0;i�;?0;[""time";o;;{ o;;["
o;
;@S;i�;0;[o;	;@S;0;["
input;i�o;>;@S;/"=;["	type;0;i�;?0;[""week";o;;{ ;	T;i�;
[o; ;i ;["padding-top;@;!;";o;,;@;"8px;;-;i�;
[ o;;i ;@;[".item-input;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;7;@�;["item-input;i�;o;;{ ;	T;i�;
[o;;i ;@;["input,
  textarea;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;	;@�;0;["
input;i�;o;;{ o;;["
o;
;@�;i�;0;[o;	;@�;0;["textarea;i�;o;;{ ;	T;i�;
[o; ;i ;["
width;@;!;";o;,;@;"	100%;;-;i�;
[ o;;i ;@;["textarea;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;	;@�;0;["textarea;i�;o;;{ ;	T;i�;
[o; ;i ;["padding-left;@;!;";o;,;@;"0;;-;i�;
[ o;3;4[o;*	;i�;"input-color-placeholder;@;+"input_color_placeholdero;&;'["px;i�;@;("	-3px;i�;)[ ;"placeholder;i�;@;
[ ;50;6{ o;
;i�;@;
[ ;;;["1/* Reset height since textareas have rows */o;;i ;@;["textarea;o;;" ;i�;[o;;[o;
;@;i�;0;[o;	;@;0;["textarea;i�;o;;{ ;	T;i�;
[o; ;i ;["height;@;!;";o;,;@;"	auto;;-;i�;
[ o;
;i�;@;
[ ;;;["/* Everything else */o;;i ;@;["7textarea,
input[type="text"],
input[type="password"],
input[type="datetime"],
input[type="datetime-local"],
input[type="date"],
input[type="month"],
input[type="time"],
input[type="week"],
input[type="number"],
input[type="email"],
input[type="url"],
input[type="search"],
input[type="tel"],
input[type="color"];o;;" ;i�;[o;;[o;
;@ ;i�;0;[o;	;@ ;0;["textarea;i�;o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""text";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""password";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""datetime";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""datetime-local";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""date";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""month";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""time";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""week";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""number";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""email";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;["
"url";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""search";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;["
"tel";o;;{ o;;["
o;
;@ ;i�;0;[o;	;@ ;0;["
input;i�o;>;@ ;/"=;["	type;0;i�;?0;[""color";o;;{ ;	T;i�;
[o; ;i ;["border;@;!;";o;,;@;"0;;-;i�;
[ o;
;i ;@;
[ ;;;["0/* Position radios and checkboxes better */o;;i ;@;["0input[type="radio"],
input[type="checkbox"];o;;" ;i;[o;;[o;
;@;i;0;[o;	;@;0;["
input;io;>;@;/"=;["	type;0;i;?0;[""radio";o;;{ o;;["
o;
;@;i;0;[o;	;@;0;["
input;io;>;@;/"=;["	type;0;i;?0;[""checkbox";o;;{ ;	T;i;
[o; ;i ;["margin;@;!;";o;,;@;"0;;-;i;
[ o; ;i ;["line-height;@;!;";o;,;@;"normal;;-;i;
[ o;
;i;@;
[ ;;;["C/* Reset width of input images, buttons, radios, checkboxes */o;;i ;@;[".item-input;o;;" ;i;[o;;[o;
;@P;i;0;[o;7;@P;["item-input;i;o;;{ ;	T;i;
[o;;i ;@;["�input[type="file"],
  input[type="image"],
  input[type="submit"],
  input[type="reset"],
  input[type="button"],
  input[type="radio"],
  input[type="checkbox"];o;;" ;i;[o;;[o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""file";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""image";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""submit";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""reset";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""button";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""radio";o;;{ o;;["
o;
;@`;i;0;[o;	;@`;0;["
input;io;>;@`;/"=;["	type;0;i;?0;[""checkbox";o;;{ ;	T;i;
[o; ;i ;["
width;@;!;";o;,;@;"	auto;;-;i;
[ o;
;i;@;
[ ;;;["-/* Override of generic input selector */o;
;i;@;
[ ;;;["6/* Set the height of file to match text inputs */o;;i ;@;["input[type="file"];o;;" ;i;[o;;[o;
;@�;i;0;[o;	;@�;0;["
input;io;>;@�;/"=;["	type;0;i;?0;[""file";o;;{ ;	T;i;
[o; ;i ;["line-height;@;!;";o;*	;i;"input-height-base;@;+"input_height_base;i;
[ o;
;i;@;
[ ;;;[">/* Text input classes to hide text caret during scroll */o;;i ;@;["U.previous-input-focus,
.cloned-text-input + input,
.cloned-text-input + textarea;o;;" ;i;[o;;[o;
;@	;i;0;[o;7;@	;["previous-input-focus;i;o;;{ o;;[	"
o;
;@	;i;0;[o;7;@	;["cloned-text-input;i;o;;{ "+o;
;@	;i;0;[o;	;@	;0;["
input;i;o;;{ o;;[	"
o;
;@	;i;0;[o;7;@	;["cloned-text-input;i;o;;{ "+o;
;@	;i;0;[o;	;@	;0;["textarea;i;o;;{ ;	T;i;
[o; ;i ;["position;@;!;";o;,;@;"absolute !important;;-;i;
[ o; ;i ;["	left;@;!;";o;&;'["px;i;@;("-9999px;i���;)[ ;i;
[ o; ;i ;["
width;@;!;";o;,;@;"
200px;;-;i;
[ o;
;i#;@;
[ ;;;["9/* Placeholder
 * ------------------------------- */o;;i ;@;["input,
textarea;o;;" ;i&;[o;;[o;
;@R	;i&;0;[o;	;@R	;0;["
input;i&;o;;{ o;;["
o;
;@R	;i&;0;[o;	;@R	;0;["textarea;i&;o;;{ ;	T;i&;
[o;3;4[ ;"placeholder;i';@;
[ ;50;6{ o;
;i+;@;
[ ;;;["</* DISABLED STATE
 * ------------------------------- */o;
;i.;@;
[ ;;;["(/* Disabled and read-only inputs */o;;i ;@;["�input[disabled],
select[disabled],
textarea[disabled],
input[readonly]:not(.cloned-text-input),
textarea[readonly]:not(.cloned-text-input),
select[readonly];o;;" ;i4;[o;;[o;
;@y	;i4;0;[o;	;@y	;0;["
input;i4o;>;@y	;/0;["disabled;0;i4;?0;0;o;;{ o;;["
o;
;@y	;i4;0;[o;	;@y	;0;["select;i4o;>;@y	;/0;["disabled;0;i4;?0;0;o;;{ o;;["
o;
;@y	;i4;0;[o;	;@y	;0;["textarea;i4o;>;@y	;/0;["disabled;0;i4;?0;0;o;;{ o;;["
o;
;@y	;i4;0;[o;	;@y	;0;["
input;i4o;>;@y	;/0;["readonly;0;i4;?0;0o;:
;@y	;["not;i4;;;;<[".cloned-text-input;o;;{ o;;["
o;
;@y	;i4;0;[o;	;@y	;0;["textarea;i4o;>;@y	;/0;["readonly;0;i4;?0;0o;:
;@y	;["not;i4;;;;<[".cloned-text-input;o;;{ o;;["
o;
;@y	;i4;0;[o;	;@y	;0;["select;i4o;>;@y	;/0;["readonly;0;i4;?0;0;o;;{ ;	T;i4;
[o; ;i ;["background-color;@;!;";o;*	;i5;"input-bg-disabled;@;+"input_bg_disabled;i5;
[ o; ;i ;["cursor;@;!;";o;,;@;"not-allowed;;-;i6;
[ o;
;i8;@;
[ ;;;["+/* Explicitly reset the colors here */o;;i ;@;["�input[type="radio"][disabled],
input[type="checkbox"][disabled],
input[type="radio"][readonly],
input[type="checkbox"][readonly];o;;" ;i<;[	o;;[o;
;@�	;i<;0;[o;	;@�	;0;["
input;i<o;>;@�	;/"=;["	type;0;i<;?0;[""radio"o;>;@�	;/0;["disabled;0;i<;?0;0;o;;{ o;;["
o;
;@�	;i<;0;[o;	;@�	;0;["
input;i<o;>;@�	;/"=;["	type;0;i<;?0;[""checkbox"o;>;@�	;/0;["disabled;0;i<;?0;0;o;;{ o;;["
o;
;@�	;i<;0;[o;	;@�	;0;["
input;i<o;>;@�	;/"=;["	type;0;i<;?0;[""radio"o;>;@�	;/0;["readonly;0;i<;?0;0;o;;{ o;;["
o;
;@�	;i<;0;[o;	;@�	;0;["
input;i<o;>;@�	;/"=;["	type;0;i<;?0;[""checkbox"o;>;@�	;/0;["readonly;0;i<;?0;0;o;;{ ;	T;i<;
[o; ;i ;["background-color;@;!;";o;,;@;"transparent;;-;i=;
[ 